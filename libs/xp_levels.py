"""
Lamastibot
Copyright (C) 2018  Sakiut

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

contact@sakiut.fr
"""


def get_levels():
    levels = [
        0,
        15,
        35,
        50,
        70,
        100,
        135,
        190,
        265,
        370,
        520,
        725,
        1015,
        1420,
        2000,
        2790,
        3900,
        5460,
        7650,
        10715,
        15000,
        18300,
        20150,
        22000,
        24000,
        26100,
        28450,
        31000,
        33800,
        36850,
        40150,
        43750,
        47700,
        52000,
        56650,
        61800,
        67350,
        73400,
        80000,
        87200,
        95000,
        103600,
        113000,
        123000,
        134150,
        146250,
        159400,
        175750,
        190000,
        206450,
        225000
    ]
    return levels


def get_level_roles():
    roles = [
        476887574208839690,
        476887576179900416,
        476887577404768267,
        476887579107786753,
        476887581229842442,
        476887582958157835,
        476887604722139136,
        476887606358048769,
        476887608165793832,
        476887609755566099,
        476887611399471104,
        476887628701237270,
        476887630609645569,
        476887632341630978,
        476887634048843789,
        476887635475038209,
        476887653640306688,
        476887655397720064,
        476887657054601226,
        476887658656956416,
        476887660346998804,
        476887677627662358,
        476887679959826433,
        476887682069561354,
        476887691632574465,
        476887693599571988,
        476887702483107844,
        476887704500699164,
        476887706153254912,
        476887707751153664,
        476887709332275210,
        476887730949718017,
        476887732958789653,
        476887734775054337,
        476887736209637409,
        476887738566574098,
        476887760343400480,
        476887762424037386,
        476887763883655179,
        476887766248980490,
        476887767717249026,
        476887789653458956,
        476887791263809547,
        476887793122148363,
        476887794774704144,
        476887796846690304,
        476887818719854612,
        476887820389056532,
        476887822490533908,
        476887824268918814,
        476887825820942349,
    ]
    return roles


def get_level_coins():

    coins = [
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        3,
        3,
        3,
        3,
        3,
        3,
        3,
        3,
        3,
        3,
        4,
        4,
        4,
        4,
        4,
        4,
        4,
        4,
        4,
        4,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
    ]

    return coins
