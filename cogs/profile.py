"""
Lamastibot
Copyright (C) 2018  Sakiut

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

contact@sakiut.fr
"""

import discord
from discord.ext import commands
from discord.ext.commands.help import Paginator

from libs import db, lib, checks
from libs.xp_levels import *

from datetime import datetime
from datetime import timedelta
from time import gmtime, strftime


class Profile(commands.Cog, name="Profil"):

    def __init__(self, bot):
        self.bot = bot

    @checks.no_pm()
    @commands.command(aliases=["info", "profil"])
    async def profile(self, ctx, *, user: discord.Member = None):
        """Affiche le profil d'un utilisateur

        Par défaut, la commande vise l'utilisateur

        Exemples :
        Retourner son propre profil :
            !profile
        Retourner le profil d'un autre utilisateur :
            !profile @pseudo
        """

        if user is None:
            user = ctx.author

        data = db.get_profile(user)

        profile_embed = discord.Embed()
        profile_embed.title = f"Profil Utilisateur pour {user} ({user.id})"
        profile_embed.colour = 0x3498db
        profile_embed.set_thumbnail(url=user.avatar_url)
        profile_embed.add_field(name="Niveau", value=f"{data['level']}")
        profile_embed.add_field(name="Points d'expérience", value=f"{data['xp_points']}")
        profile_embed.add_field(name="_ _", value="_ _", inline=False)
        profile_embed.set_footer(text=f"Requested by {ctx.message.author} | "
        f"{strftime('%a, %d %b %Y %H:%M:%S')}",
                                 icon_url=ctx.message.author.avatar_url)

        level = data['level']
        xp_levels = get_levels()
        actual_level_xp = xp_levels[level]

        if level == 50:
            next_level_xp = xp_levels[level]
        else:
            next_level_xp = xp_levels[level + 1]
        profile_embed.add_field(name="Par rapport aux niveaux précédent et suivant :", value="_ _", inline=False)
        profile_embed.add_field(name="XP obtenue",
                                value=f"{data['xp_points'] - actual_level_xp}")
        profile_embed.add_field(name="XP restante",
                                value=f"{next_level_xp - data['xp_points']}")
        profile_embed.add_field(name="_ _", value="_ _", inline=False)
        profile_embed.add_field(name="Lamasticoins", value=data["coins"])
        profile_embed.add_field(name="❄", value=data["flakes"])

        try:
            await ctx.author.send(embed=profile_embed)
        except discord.Forbidden:
            await ctx.send(embed=profile_embed)

    @checks.no_pm()
    @commands.command(aliases=['classement', 'ranking'])
    async def top(self, ctx, limit=10):
        """Affiche le classement décroissant des niveaux du serveur

        Affiche 10 noms de base, peut être étendu à 25

        Exemples :
        Top 10
            !top
        Top 15
            !top 15"""

        await ctx.message.delete()

        if int(limit) > 25:
            await ctx.send("Vous ne pouvez pas afficher plus que le top 25", delete_after=5)
            return

        top = db.get_top()
        user_profile = db.get_profile(ctx.author)
        try:
            user_place = top.index(user_profile) + 1
        except ValueError:
            user_place = "100+"

        fmt = Paginator(prefix="```python")
        i = 0

        for entry in top:
            i += 1
            user_id = entry['discord_uuid']
            user = ctx.guild.get_member(user_id)

            if user is None:
                user_name = f"Membre parti ({user_id})"
            else:
                user_name = f"{user.name}#{user.discriminator}"

            if i > limit:
                break

            fmt.add_line(f"{i:>2}. {user_name:42}\n    Level : {entry['level']:>4}  XP : {entry['xp_points']:10}")
            fmt.add_line(empty=True)

        fmt.add_line("----------------------------------------------")
        fmt.add_line("# Vos statistiques")
        fmt.add_line(f"Rang: {user_place:5}  Level: {user_profile['level']:3}  XP: {user_profile['xp_points']:<10}")
        fmt.close_page()

        for page in fmt.pages:
            await ctx.send(page)

    @checks.no_pm()
    @checks.admin_or_permissions()
    @commands.command(aliases=['givexp', 'xp_give', 'xpgive', 'give_exp'])
    async def give_xp(self, ctx, user: discord.Member, amount: int = None):
        """Ajouter de l'XP à un utilisateur

        Utilisable par les admins uniquement

        -----------------------------------------------------------------------

        Utilisation :
            !give_xp <Utilisateur (mention ou nom)> <quantité à give (entier)>

        Exemple :
            !give_xp @pseudo 42

        -----------------------------------------------------------------------

        Pour enlever de l'XP à un utilisateur, il suffit de mettre un montant
        négatif

        Exemple :
            !give_xp @pseudo -42"""

        await ctx.message.delete()

        if amount is None:
            await ctx.send(f"```Vous devez saisir un montant```", delete_after=5)

        if amount == 0:
            await ctx.send(f"```Cannot add 0 xp to an user```", delete_after=5)
            return

        if user is None:
            user = ctx.author

        profile = db.get_profile(user)
        if profile is None:
            profile = db.create_profile(user)

        profile['xp_points'] += amount

        db.update_profile(user.id, profile)

        if amount > 0:
            await ctx.send(f"```Successfully added {amount} xp points to {user} !```", delete_after=5)
        elif amount < 0:
            await ctx.send(f"```Successfully retired {-amount} xp points from {user} !```", delete_after=5)

    @checks.no_pm()
    @checks.admin_or_permissions()
    @commands.command(aliases=['givecoins', 'coins_give', 'coingive', 'give_coin'])
    async def give_coins(self, ctx, user: discord.Member, amount: int = None):
        """Ajouter un ou plusieurs coins à un utilisateur

        Utilisable par les admins uniquement

        -------------------------------------------------------------------------

        Utilisation :
            !give_coins <Utilisateur (mention ou nom)> <quantité à give (entier)>

        Exemple :
            !give_coins @pseudo 42

        -------------------------------------------------------------------------

        Pour enlever un ou plusieurs coins à un utilisateur, il suffit de mettre
        un montant négatif

        Exemple :
            !give_coins @pseudo -42"""

        await ctx.message.delete()

        if amount is None:
            await ctx.send(f"```Vous devez saisir un montant```", delete_after=5)

        if amount == 0:
            await ctx.send(f"```Cannot add 0 coins to an user```", delete_after=5)
            return

        if user is None:
            user = ctx.author

        profile = db.get_profile(user)
        if profile is None:
            profile = db.create_profile(user)

        profile['coins'] += amount

        db.update_profile(user.id, profile)

        if amount > 0:
            await ctx.send(f"```Successfully added {amount} coins to {user} !```", delete_after=5)
        elif amount < 0:
            await ctx.send(f"```Successfully retired {-amount} coins from {user} !```", delete_after=5)

    # TODO: !color change cooldown: 5min

    @checks.no_pm()
    @checks.is_owner()
    @commands.command()
    async def give_coins_everyone(self, ctx, amount):

        iterator = 0

        profiles_list = db.get_all_profiles()

        if profiles_list is None:
            return

        msg = await ctx.author.send(content="Starting give...")

        if amount.lower() == "limit":

            for profile in profiles_list:
                coins_limit = get_level_coins()
                coins_limit = coins_limit[profile["level"]]

                if profile["coins"] < coins_limit:
                    profile["coins"] += 1
                elif profile["coins"] > coins_limit:
                    profile["coins"] = coins_limit
                else:
                    iterator += 1
                    await msg.edit(content=f"{iterator}/4693 done")
                    continue
                db.update_profile(profile["discord_uuid"], profile)

                iterator += 1
                await msg.edit(content=f"{iterator}/4693 done")

        else:

            for profile in profiles_list:
                profile["coins"] += 1
                db.update_profile(profile["discord_uuid"], profile)

                iterator += 1
                await msg.edit(content=f"{iterator}/4693 done")

        await msg.edit(content="Initialisation done!")

    @checks.no_pm()
    @commands.group(aliases=['colors'], invoke_without_command=True)
    async def color(self, ctx):
        """Commandes liées aux changements de couleur

        Utilisation :
            !color list
            !color change [nom de la couleur]"""

        if ctx.invoked_subcommand is None:
            await ctx.message.delete()
            await ctx.send("```md\nSyntaxe invalide. Voir !help color pour plus d'informations sur comment "
                           "utiliser cette commande.\n```")

    @checks.no_pm()
    @color.command()
    async def list(self, ctx):
        """Affiche la liste des couleurs disponibles

        Exemple:
            !color list"""

        await ctx.message.delete()
        profile = db.get_profile(ctx.author)

        fmt = "```\n"

        if profile['level'] >= 20:
            fmt += "Niveau 20 et plus : Grey\n"

        if profile['level'] >= 30:
            fmt += "Niveau 30 et plus : Turquoise\n"

        if profile['level'] >= 40:
            fmt += "Niveau 40 et plus : Candy\n"

        if profile['level'] >= 50:
            fmt += "Niveau 50 :         Red\n"

        if lib.get_role(ctx.guild, 484795749041831998) in ctx.author.roles:
            fmt += "\nMaître du temps :   Orange (TimeMaster)\n"

        if lib.get_role(ctx.guild, 251419879951958027) in ctx.author.roles:
            fmt += "\nGardien :           Blue\n"

        if lib.get_role(ctx.guild, 518427790417199113) in ctx.author.roles:
            fmt += "\nNoël 2018 :         Christmas\n"

        # if lib.get_role(ctx.guild, 349308359968686080) in ctx.author.roles:
        #     fmt += "\nWhite (subs) :      Pleb\n"

        elif profile["level"] < 20 and not (lib.get_role(ctx.guild, 484795749041831998) or
                                            lib.get_role(ctx.guild, 251419879951958027) or
                                            lib.get_role(ctx.guild, 518427790417199113)):
            # or lib.get_role(ctx.guild, 349308359968686080)
            await ctx.send("Vous ne pouvez pas changer de couleur, atteignez le niveau 20 pour en débloquer une !",
                           delete_after=5)
            return

        fmt += "```"
        await ctx.author.send(fmt)

    @checks.no_pm()
    @color.command()
    async def change(self, ctx, *, choice=None):
        """Changer de couleur

        Liste des couleurs existantes :
            - Grey
            - Turquoise
            - Candy
            - Red
            - Orange (TimeMaster)
            - Blue (Gardiens uniquement)
            - Christmas (Pour les gagnants de l'event de Noël)

        Remplir le champ couleur avec "off" enlèvera toutes vos couleurs
        et vous permettra de retrouver votre teinte normale

        Exemples:
            !color change timemaster
            !color change grey
            !color change off"""

        await ctx.message.delete()

        if choice is None:
            await ctx.send("Vous devez préciser votre choix", delete_after=5)
            return

        profile = db.get_profile(ctx.author)

        choices = {
            "grey": [487637704021114924, False],
            "turquoise": [487641670972866561, False],
            "candy": [487642024984707073, False],
            "red": [487642604465422336, False],
            "timemaster": [488010490526826529, False],
            "gardien": [508285186321022998, False],
            # "pleb": [522139983348367360, False],
            "christmas": [516260823774134293, False],
        }

        if profile['level'] >= 20:
            choices["grey"][1] = True

        if profile['level'] >= 30:
            choices["turquoise"][1] = True

        if profile['level'] >= 40:
            choices["candy"][1] = True

        if profile['level'] >= 50:
            choices["red"][1] = True

        time_master = lib.get_role(ctx.guild, 484795749041831998)
        if time_master in ctx.author.roles:
            choices["timemaster"][1] = True

        gardien = lib.get_role(ctx.guild, 251419879951958027)
        if gardien in ctx.author.roles:
            choices["gardien"][1] = True

        christmas = lib.get_role(ctx.guild, 518427790417199113)
        if christmas in ctx.author.roles:
            choices["christmas"][1] = True

        # sub = lib.get_role(ctx.guild, 349308359968686080)
        # if sub in ctx.author.roles:
        #     choices["pleb"][1] = True

        elif profile["level"] < 20 and not (lib.get_role(ctx.guild, 484795749041831998) or
                                            lib.get_role(ctx.guild, 251419879951958027) or
                                            lib.get_role(ctx.guild, 518427790417199113)):
            # or lib.get_role(ctx.guild, 349308359968686080)
            await ctx.send("Vous n'avez débloqué aucune couleur pour le moment !",
                           delete_after=5)
            return

        if choice.lower() == "orange":
            choice = "timemaster"

        if choice.lower() in ("blue", "bleu"):
            choice = "gardien"

        # if choice.lower() == "white":
        #     choice = "pleb"

        await lib.delete_color_roles(ctx.author)

        if choice.lower() == "off":

            await ctx.author.send("Toutes vos couleurs vous ont été retirées")
            return

        try:
            if choices[choice.lower()][1] is True:
                await ctx.author.add_roles(lib.get_role(ctx.guild, choices[choice.lower()][0]))
                await ctx.author.send("Nouvelle couleur attribuée !")
            else:
                await ctx.send("Vous n'avez pas le droit de vous give cette couleur, vous n'avez pas le niveau requis",
                               delete_after=5)
                return
        except IndexError:
            await ctx.send(f"Couleur {choice} non trouvée", delete_after=5)
            return

    @checks.no_pm()
    @checks.admin_or_permissions()
    @commands.command()
    async def delete_profile(self, ctx, discord_uuid):
        """Supprimer un profil utilisateur

        Exemple d'utilisation :
            !delete_profile 121928946320146432
        """

        if db.get_profile(user_id=discord_uuid) is None:
            await ctx.send("Le profil utilisateur n'existe pas")
        else:
            db.delete_profile(discord_uuid)
            await ctx.send("Profil supprimé avec succès !")

    @checks.no_pm()
    @commands.command(aliases=['profile_update', 'updateprofile', 'profileupdate', 'fixid'])
    async def update_profile(self, ctx, user: discord.Member = None):
        """Mettre à jour son profil

        Exemple d'utilisation :
            !update_profile

        ------------------------------------------------------

        Pour les Gardiens et admins :

        Mettre à jour le profil de n'importe quel utilisateur

        Exemple d'utilisation :
            !update_profile @pseudo
        """

        checked_member = ctx.author

        # Checking permissions if there is a user mentioned in arguments

        if user is not None:

            if not checks.role_or_permissions(ctx, lambda r: r.id == 251419879951958027, manage_roles=True):
                await ctx.send("Erreur 403 : Permissions insuffisantes - Commande refusée", delete_after=5)

            checked_member = user

        # LEVEL ROLES

        # Retrieving data

        profile = db.get_profile(user=checked_member)
        xp_roles = get_level_roles()
        level = []

        # Determining the role's level of the user

        for role in checked_member.roles:
            if role.id in xp_roles:
                level.append(xp_roles.index(role.id))

        try:
            level = level[0]
        except IndexError:
            level = 0

        # Fixing error if discovered

        if level != profile["level"]:

            level = profile["level"]

            await lib.delete_level_roles(checked_member)
            await checked_member.add_roles(lib.get_role(ctx.guild, xp_roles[level]))

        # LAMASTICOINS

        # Converting last coin date

        last_coin = profile['last_coin']
        fmt = "%Y-%m-%d"
        last_coin = datetime.strptime(last_coin, fmt)

        # Comparing dates

        if datetime.today().strftime(fmt) != last_coin.strftime(fmt):

            # Checking coins

            coins_limit = get_level_coins()
            coins_limit = coins_limit[profile["level"]]

            # Fixing error if discovered

            if profile["coins"] < coins_limit:
                profile["coins"] += 1
                profile["last_coin"] = datetime.today().strftime('%Y-%m-%d')
            elif profile["coins"] > coins_limit:
                profile["coins"] = coins_limit

        # SPECIAL ROLES

        # Checking roles

        if not (lib.get_role(ctx.guild, 518427790417199113) or lib.get_role(ctx.guild, 484795749041831998)) \
                in checked_member.roles:

            if profile['flakes'] == 1000:                                                       # Verifying DB
                await checked_member.add_roles(lib.get_role(ctx.guild, 518427790417199113))     # Adding role

            if profile['time_master'] is True:                                                  # Verifying DB
                await checked_member.add_roles(lib.get_role(ctx.guild, 484795749041831998))     # Adding role

        db.update_profile(checked_member.id, profile)

        await ctx.send("Vos informations ont été actualisées avec succès !")
