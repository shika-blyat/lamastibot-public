# __**Changelog**__

## __**Mise à jour du 01/09/2018**__

**1. Déploiement de la nouvelle version du Discord**

Annonce de la version 2.0 du Discord

**2. Ajout du système de niveaux**

- Implémentation d'un système de niveau dépendant de l'activité et pas seulement du nombre de message (spammer ne sert à rien)
- Création de la commande `!profile` qui permet d'afficher son profil (channel `#bots-commands` uniquement)
- Création de la commande `!top` qui permet d'afficher un top 10 des plus hauts level (channel `#bots-commands` uniquement)

**3. Réunification des salons textuels**

## __**Mise à jour du 08/09/2018**__

Modification de l'algorithme d'xp : suppression du comptage des liens et des messages n'ayant aucun sens

## __**Mise à jour du 08/09/2018**__

Modification de l'algorithme d'xp : les messages constitués uniquement d'émojis ne sont plus comptabilisés

## __**Mise à jour du 08/10/2018**__

Lancement du Fourasticot ! Enjoy !

## __**Mise à jour du 08/11/2018**__

Lancement du Lamasticoin
Les Gardiens peuvent désormais changer de couleur !
Quelques bug fix

## __**Mise à jour du 01/12/2018**__

- Lancement de **l'event Noël 2018**
- Implémentation du système de **flocons**
- Ajout **d'une seconde** au délai du Fourasticot (7 → 8)

- Modifications sur l'algo d'xp
- Multiples corrections de bugs

## __**Mise à jour du 26/12/2018**__

- Ajout d'une commande `!bug` pour report les éventuels bugs. Plus d'informations avec la commande `!help bug` ou dans les épinglés du channel #bot-commands

## __**Mise à jour du 01/05/2019**__

**Bugs :**

- **Changement de couleur** fixé (notamment le `!color change off`) - Reported by `@Rolf La Gentil Pingouine 🐧#6785` and `@Alennah#9524` 
- Amélioration de l'attibution des **Lamasticoins** - Reported by `@Alennah#9524`, `@Guitard Akoustikk#0233`, `@Dropout#8943`and `@Bellek#8650`
- Améliorations globales de performance et **optimisation** - Thanks `@Kao#2541` pour le coup de main

**Nouveautés :**

- Création d'une commande **`!update_profile`** (alias **`!fixid`**) qui permet de refresh son profil par rapport à la base de données. En gros ça remet les bons rôles et **remet les Lamasticoin quand on les a pas eu**
- Préparation du terrain pour de futur events