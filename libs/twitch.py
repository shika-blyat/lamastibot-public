# -*- coding:utf-8 -*-

"""
Twitch API's library

Lamastibot
Copyright (C) 2018  Sakiut

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

contact@sakiut.fr
"""

import requests
from libs import lib


def start(client_id, secret_key):

    stream = False
    stream_id = None

    parameters = {
        "client_id": client_id,
        "client_secret": secret_key,
        "grant_type": "client_credentials"
    }

    req = requests.post("https://api.twitch.tv/kraken/oauth2/token", params=parameters)
    if req.status_code == 200:
        req = req.json()

    access_token = req["access_token"]

    parameters = {
        "client_id": client_id,
        "token": access_token,
        "channel": "corobizar"
    }

    req = requests.get("https://api.twitch.tv/kraken/streams", params=parameters)
    if req.status_code == 200:
        req = req.json()

    if req["_total"] > 0:
        stream = True
        stream_id = req["streams"][0]["_id"]

    return stream, stream_id


def check(client_id, secret_key, original_stream_id):

    stream = False
    stream_id = original_stream_id
    ping = False
    data = None

    parameters = {
        "client_id": client_id,
        "client_secret": secret_key,
        "grant_type": "client_credentials"
    }

    req = requests.post("https://api.twitch.tv/kraken/oauth2/token", params=parameters)
    if req.status_code == 200:
        req = req.json()
    else:
        raise ConnectionError("Cannot connect to database")

    access_token = req["access_token"]

    parameters = {
        "client_id": client_id,
        "token": access_token,
        "channel": "corobizar"
    }

    req = requests.get("https://api.twitch.tv/kraken/streams", params=parameters)
    if req.status_code == 200:
        req = req.json()

    if req["_total"] > 0:
        stream = True
        stream_id = req["streams"][0]["_id"]
        if stream_id != original_stream_id:
            ping = True
        data = req["streams"][0]

    return stream, stream_id, ping, data
