"""
Lamastibot
Copyright (C) 2018  Sakiut

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

contact@sakiut.fr
"""

import discord
import asyncio
from discord.ext import commands
from datetime import datetime, timedelta

from libs import captcha as captcha_mgr
from libs import lib, db


print(f"[F][{datetime.now().strftime('%x %X')}] Connecting...")


class Bot(commands.AutoShardedBot):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, activity=discord.Game(name="À quand la prochaine épreuve ?"), **kwargs)

        self.owner_id = 187565415512276993
        self.link_channel_id = 484481286015287297
        self.lamastibot_id = 352425465195134976

        self.uptime = datetime.now()

    async def on_ready(self):
        print(f"""-------------------------------------------------------------
                  [F][{datetime.now().strftime("%x %X")}] Logged in as
                  [F][{datetime.now().strftime('%x %X')}]
                  [F][{datetime.now().strftime('%x %')}]
                  -------------------------------------------------------------""", self.user.name, self.user.id)

    async def on_message(self, message):

        if not message.channel.id == self.link_channel_id:
            return

        if message.author.id not in (self.owner_id, self.lamastibot_id):
            return

        task_type = message.content.split(":")[0]

        if task_type == "start":
            content = message.content.split(":")[1].split("|-@-|")
            data = {
                "channel_id": int(content[0]),
                "captcha_id": content[1],
            }

            channel = self.get_channel(data["channel_id"])
            captcha = captcha_mgr.get_captcha(data['captcha_id'])

            def check(m):
                return m.channel == channel

            await channel.send("""<:FOURASTICOT:230222304737492992> Voici mon épreuve, à vos claviers ! 
                               <:FOURASTICOT:230222304737492992>""",
                               file=discord.File(captcha),
                               delete_after=600)
            end_time = datetime.now() + timedelta(seconds=8)

            results = []
            losers = []

            try:
                while datetime.now() < end_time:
                    rs = await self.wait_for("message", check=check, timeout=8)
                    if lib.is_time_master(rs.author, rs.guild):
                        await channel.send(f"""Tu as déjà réussi mon épreuve {rs.author.mention}, laisse les autres 
                                           tenter leur chance !  <:yee:293391531237441537>""",
                                           delete_after=600)
                        continue
                    if rs.content == data["captcha_id"]:
                        results.append(rs.author)
                    else:
                        continue
            except asyncio.TimeoutError:
                pass

            end_time = datetime.now() + timedelta(seconds=15)

            try:
                while datetime.now() < end_time:
                    rs = await self.wait_for("message", check=check, timeout=15)
                    if rs.content == data["captcha_id"]:
                        losers.append(rs.author)
                    else:
                        continue
            except asyncio.TimeoutError:
                pass

            if len(results) == 0:
                await channel.send("""Aucun disciple n'a réussi à me vaincre, entraînez-vous d'ici mon prochain passage !
                                    <:CACA:230222568441774081>""",
                                   delete_after=600)
                for user in losers:
                    await lib.lose_rename(user, lib.get_role(message.guild, 487642902604939264))

                return

            winner = results[0]

            await winner.add_roles(lib.get_time_master(channel.guild))
            db.add_xp(winner, 750)
            await channel.send(f"""Nous avons notre gagnant ! GG {winner.mention}, tu as réussi mon épreuve ! 
Voici tes récompenses : <:FOURASTICOT:230222304737492992>
- Le grade "Maître du Temps"
- 750 points d'expérience
- La couleur "Orange" """,
                               delete_after=600)

            winner_profile = db.get_profile(user=winner)
            winner_profile["time_master"] = True
            db.update_profile(winner.id, winner_profile)

            del results[0]

            for user in results:
                await lib.lose_rename(user, lib.get_role(message.guild, 487642902604939264))

            for user in losers:
                await lib.lose_rename(user, lib.get_role(message.guild, 487642902604939264))

        await self.process_commands(message)


token = lib.get_fourasticot_token()
bot = Bot(command_prefix=commands.when_mentioned_or(".!"), description="Commandes Fourasticot", pm_help=True)
bot.run(token)
