"""
Lamastibot
Copyright (C) 2018  Sakiut

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

contact@sakiut.fr
"""

from datetime import datetime, timedelta, date
import asyncio
import random

import discord
import discord.errors
from discord.ext import commands
from discord.ext.commands.help import Paginator

from libs import checks, lib, twitch, db, fourasticot, captcha
from libs import blacklist
from libs.xp_levels import *

if not discord.opus.is_loaded():
    # the 'opus' library here is opus.dll on windows
    # or libopus.so on linux in the current directory
    # you should replace this with the location the
    # opus library is located in and with the proper filename.
    # note that on windows this DLL is automatically provided for you
    discord.opus.load_opus('opus')

print(f'[L][{datetime.now().strftime("%x %X")}] Connecting...')


class Bot(commands.AutoShardedBot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, activity=discord.Game(name="Corobizar.com | !help"), **kwargs)

        # General variables

        self.owner_id = 187565415512276993
        self.launch_time = datetime.now()
        self.date = date.today()
        self.blocked_list = db.get_xp_blocked()
        self.xp_boost = db.get_boost()["boost"]

        # Debug

        self.debug = False
        self.debug_channel = None

        # Cogs

        from cogs.settings import Settings, Issues
        from cogs.profile import Profile
        from cogs.moderation import Moderation
        from cogs.events import Events

        self.add_cog(Moderation(self))
        self.add_cog(Profile(self))
        self.add_cog(Settings(self))
        self.add_cog(Events(self))
        self.add_cog(Issues(self))

        # Stream detection

        self.stream_id = None

        # Fourasticot

        self.fourasticot_channels = lib.get_everyone_channels()
        self.next_fourasticot = datetime.utcnow() + timedelta(minutes=random.randint(59, 150))

        # Blacklist init

        self.blacklist = blacklist.get()

    async def on_ready(self):
        print('-------------------------------------------------------------')
        print(f'[L][{datetime.now().strftime("%x %X")}] Logged in as')
        print(f'[L][{datetime.now().strftime("%x %X")}]', self.user.name)
        print(f'[L][{datetime.now().strftime("%x %X")}]', self.user.id)
        print('-------------------------------------------------------------')

        # Loops' run

        while True:
            await self.check_temp(self.get_guild(220309354266624000))
            await self.stream_start_check(self.get_channel(220585726423728130))
            await self.fourasticot_check()
            self.coin_check()
            await asyncio.sleep(30)

    async def on_member_ban(self, guild, member):

        self.debug_channel = guild.get_channel(595653106562498560)

        act = discord.AuditLogAction.ban

        def predicate(event):
            return event.target == member and event.action == act

        entry = await guild.audit_logs().find(predicate)

        debug_embed = db.create_entry(
            bot=self,
            user=member,
            sanction="Ban",
            channel="N/A",
            author=entry.user,
            date=entry.created_at,
            reason=entry.reason,
            comment="Aucun"
        )

        if self.debug:
            await self.debug_channel.send(embed=debug_embed)

    @staticmethod
    async def on_member_join(member):
        if member.bot:
            return
        profile = db.get_profile(member)
        if profile is None:
            db.create_profile(member)

    async def on_message(self, message):

        ctx = await self.get_context(message)

        if self.debug:
            self.debug_channel = message.guild.get_channel(595653106562498560)

        if message.author.bot:
            return

        if message.guild is None:
            await self.process_commands(message)
            return

        # if message.author.top_role.id in (251419879951958027, 220310157513457664):

        levels = get_levels()
        level_roles = get_level_roles()

        profile = db.get_profile(message.author)
        if profile is None:
            profile = db.create_profile(message.author)

        if message.author.id in self.blacklist:
            blacklist.save(message.author, message)

        try:
            last_message_date = profile['last_message_date'].split("+")[0]
            # last_flake = profile['last_flake'].split("+")[0]
        except TypeError:
            await self.process_commands(message)
            return

        # if last_flake is None:
        #     last_flake = last_message_date

        fmt = "%Y-%m-%dT%H:%M:%S.%f"
        # lib.crop_microseconds(last_message_date)
        try:
            last_message_date = datetime.strptime(last_message_date, fmt)
        except ValueError:
            fmt = "%Y-%m-%dT%H:%M:%S"
            last_message_date = datetime.strptime(last_message_date, fmt)

        # fmt = "%Y-%m-%dT%H:%M:%S.%f"
        # lib.crop_microseconds(last_flake)
        # try:
        #     last_flake = datetime.strptime(last_flake, fmt)
        # except ValueError:
        #     fmt = "%Y-%m-%dT%H:%M:%S"
        #     last_flake = datetime.strptime(last_flake, fmt)

        if message.content[1:] == message.content[:-1]:
            await self.process_commands(message)
            return

        if message.content.lower().startswith("https://") or message.content.lower().startswith("http://"):
            await self.process_commands(message)
            return

        if message.content.lower().startswith("<:") and message.content.lower().endswith(">") \
                and message.content.lower().count(":") == 2:
            await self.process_commands(message)
            return

        if message.author.id in self.blocked_list:
            await self.process_commands(message)
            return

        # christmas_role = lib.get_role(ctx.guild, 518427790417199113)

        # if last_flake.day < message.created_at.day:
        #     profile["today_flakes"] = 50
        #     profile["flakes"] += 50
        #     profile["msgs_before_last_flake"] = 0
        #     profile["last_flake"] = message.created_at
        #     if not (christmas_role in message.author.roles):
        #         await message.author.send("En vous réveillant ce matin, vous récoltez 50 flocons !")
        #
        # elif (message.created_at - last_flake) > timedelta(minutes=random.randint(8, 12)) \
        #         and len(message.content) >= 3 \
        #         and profile["msgs_before_last_flake"] >= random.randint(10, 14):
        #
        #     if profile["today_flakes"] < 200:
        #         profile["today_flakes"] += 10
        #         profile["flakes"] += 10
        #         profile["msgs_before_last_flake"] = 0
        #         if profile["today_flakes"] == 200:
        #             await message.author.send("Bravo, vous avez collecté le maximum de flocons disponibles "
        #                                       "aujourd'hui !")
        #
        #     profile["last_flake"] = message.created_at
        #
        # else:
        #     profile["msgs_before_last_flake"] += 1
        #
        # if profile["flakes"] >= 1000:
        #     profile["flakes"] = 1000
        #
        #     if not (christmas_role in message.author.roles):
        #         await message.author.add_roles(christmas_role)
        #         await message.author.send("Bravo à toi jeune lama, tu as obtenu le rôle *Ramasseur de flocons* pour "
        #                                   "avoir été actif ce mois de décembre ! Joyeux Noël !")

        debug_embed = None

        if self.debug:
            debug_embed = discord.Embed()
            debug_embed.title = f"Starting XP attribution for {message.author}"
            debug_embed.description = "```\n"

        if last_message_date.day < message.created_at.day:
            amount = levels[profile['level'] + 1] - levels[profile['level']]
            profile['xp_points'] += int(0.02 * amount * self.xp_boost)
            profile['last_message_date'] = message.created_at

            if self.debug:
                debug_embed.description += f"FMotD Bonus : {str(int(0.02 * amount * self.xp_boost))}\n"

        elif (message.created_at - last_message_date) > timedelta(minutes=random.randint(4, 8)) \
                and len(message.content) >= 3 \
                and profile["msgs_before_last_xp"] >= random.randint(1, 2):

            profile['xp_points'] += 15 * self.xp_boost
            profile["msgs_before_last_xp"] = 0

            if self.debug:
                debug_embed.description += f"Message XP give : {15 * self.xp_boost}\n"

            profile['last_message_date'] = message.created_at

        else:
            profile["msgs_before_last_xp"] += 1
            if self.debug:
                debug_embed.description += "No XP this time\n"

        if profile['xp_points'] > 225000:
            profile['xp_points'] = 225000
            if self.debug:
                debug_embed.description += "XP Already at maximum → Back to 225000\n"

        if self.debug:
            debug_embed.add_field(name="Profile final XP", value=profile["xp_points"])
            debug_embed.add_field(name="Final messages nb", value=profile["msgs_before_last_xp"])

        if profile['xp_points'] == 15:
            level = 1
        else:
            i = 0

            for level in levels:
                if level > profile['xp_points']:
                    break
                i += 1

            level = i - 1

        if self.debug:
            debug_embed.add_field(name="Final Level", value=str(level))

        if level == profile['level']:
            pass

        else:
            # alpha_channel = self.get_channel(478037718987964427)
            profile['level'] = level
            try:
                role = list(filter(lambda x: x.id == level_roles[level], message.guild.roles))
            except AttributeError:
                await self.process_commands(message)
                return
            role = role[0]

            await lib.delete_level_roles(message.author)
            await message.author.add_roles(role)

            colors = {
                20: "grey",
                30: "turquoise",
                40: "candy",
                50: "red",
            }

            if level in (20, 30, 40):
                await message.channel.send(f"<a:PORO:486496641541603329> {message.author.mention} est désormais "
                                           f"niveau **{level}** ! GG  <a:PORO:486496641541603329> !")
                image = discord.File(f"./data/colors_source/{colors[level]}.png")
                await message.author.send(f"Vous avez atteint le niveau **{level}**, vous avez donc débloqué une "
                                          f"nouvelle couleur : {colors[level].capitalize()}", file=image)
            elif level == 50:
                soken = self.get_user(121928946320146432)
                await soken.send(f"{message.author.mention}({message.author.id}) a atteint le niveau 50"
                                 f" !")

                image = discord.File(f"./data/colors_source/{colors[level]}.png")
                await message.author.send(f"Vous avez atteint le niveau **{level}**, vous avez donc débloqué une"
                                          f"nouvelle couleur : {colors[level].capitalize()}", file=image)

                await message.channel.send(
                    f"<a:LAMACOIN:485323716390158367> Que tout le monde acclame {message.author.mention} pour avoir"
                    f" atteint l'ultime niveau 50, Félicitations ! <a:LAMACOIN:485323716390158367>"
                )
            else:
                try:
                    await message.author.send(f"Vous êtes désormais niveau **{level}** !")
                except discord.errors.Forbidden:
                    pass

        if self.debug:
            debug_embed.add_field(name="Final Last Message Date", value=f"{profile['last_message_date']}")

        return_code = db.update_profile(message.author.id, profile)

        if self.debug:
            debug_embed.description += f"DB return code : {return_code}\n\nProfile updated\n```"
            debug_embed.set_footer(icon_url=message.author.avatar_url,
                                   text=f"Message by {message.author} | {message.created_at}")
            debug_embed.colour = 0x000000
            await self.debug_channel.send(embed=debug_embed)

        try:
            if ctx.valid:
                if lib.get_role(message.guild, 251419879951958027) in message.author.roles \
                        or lib.get_role(message.guild, 220310157513457664) in message.author.roles:
                    await self.process_commands(message)
                else:
                    if message.channel.id == 485481154124840992:
                        await self.process_commands(message)
                    else:
                        await message.delete()
                        await message.channel.send(f"""{message.author.mention} Vous ne pouvez utiliser de commande ici, 
                                                    merci d'aller dans #bots-commands""", delete_after=10)
        except AttributeError:
            await self.process_commands(message)

    async def check_temp(self, guild):

        temp_sanctions = db.get_temp_sanctions()
        _date = datetime.utcnow()

        for sanction in temp_sanctions:
            until = sanction['until'].split("+")[0]
            fmt = "%Y-%m-%dT%H:%M:%S.%f"
            # lib.crop_microseconds(until)
            try:
                until = datetime.strptime(until, fmt)
            except ValueError:
                fmt = "%Y-%m-%dT%H:%M:%S"
                until = datetime.strptime(until, fmt)

            user = db.get_user(sanction)
            user = self.get_user(user['discord_uuid'])

            if _date < until:
                continue
            else:
                try:
                    if sanction['sanction'] == "Ban":
                        await guild.unban(user, reason="Sanction expirée")
                        await user.send("Vous avez été unban de la Flotte de Corobizar, raison : « Sanction expirée ».")
                    elif sanction['sanction'] == "Mute (text)":
                        if sanction['channel'] is None:
                            for chan in guild.text_channels:
                                await chan.set_permissions(user, overwrite=None)
                            await user.send("""Vous avez été unmute de tous les channels de la Flotte de Corobizar, 
                                            raison : « Sanction expirée ».""")
                        else:
                            channel = self.get_channel(sanction["channel"])
                            await channel.set_permissions(user, overwrite=None)
                            await user.send(f"""Vous avez été unmute du channel #{channel.name} de la Flotte de Corobizar
                                            , raison : « Sanction expirée ».""")
                    elif sanction['sanction'] == "Block XP":
                        self.blocked_list = [x for x in self.blocked_list if x != user.id]
                        db.save_xp_blocked(self.blocked_list)
                        await user.send(f"""Vous pouvez à nouveau engranger de l'expérience sur la Flotte de Corobizar,
                                        raison : « Sanction expirée ».""")

                    elif sanction['sanction'] == "Loser Rename":
                        user = guild.get_member(user.id)
                        await user.edit(nick=None)
                        await user.remove_roles(lib.get_role(guild, 487642902604939264))
                    else:
                        continue
                except discord.errors.Forbidden:
                    pass

                print(f"Temp Sanction deleted : {db.delete_temp_sanction(sanction['uuid'])} - {sanction['sanction']}")

    async def stream_start_check(self, channel):

        client_id = lib.get_twitch_client_id()
        secret_key = lib.get_twitch_secret_key()

        check_result = twitch.check(client_id, secret_key, self.stream_id)
        # stream = check_result[0]
        self.stream_id = check_result[1]
        ping = check_result[2]
        data = check_result[3]

        if ping:
            stream_embed = discord.Embed()
            stream_embed.title = "STREAM ON !"
            stream_embed.colour = 0x3498db
            stream_embed.description = """Un nouveau stream a été lancé, rejoins-nous vite !
                                       [Clique ici ! Oui, clique !](https://twitch.tv/corobizar/)"""
            stream_embed.add_field(name="Jeu", value=data["game"])
            stream_embed.add_field(name="Viewers", value=data["viewers"])
            stream_embed.set_image(url=data['preview']['large'])
            stream_embed.set_thumbnail(url=data['channel']['logo'])

            await channel.send("@everyone", embed=stream_embed)

    async def fourasticot_check(self):

        if datetime.utcnow() < self.next_fourasticot:
            return
        else:
            await fourasticot.start(self, random.choice(self.fourasticot_channels),
                                    random.choice(captcha.get_listing()))
            self.next_fourasticot = datetime.utcnow() + timedelta(minutes=random.randint(59, 250))

    def coin_check(self):
        
        if self.date == date.today():
            return
        else:
            self.date = date.today()

        # print("Starting initialisation...")
        # iterator = 0

        profiles_list = db.get_all_profiles()

        if profiles_list is None:
            return

        for profile in profiles_list:
            coins_limit = get_level_coins()
            coins_limit = coins_limit[profile["level"]]

            if profile["coins"] < coins_limit:
                profile["coins"] += 1
                profile["last_coin"] = datetime.today()
            elif profile["coins"] > coins_limit:
                profile["coins"] = coins_limit
            else:
                # iterator += 1
                # print(f"{iterator}/4693 done")
                continue
            db.update_profile(profile["discord_uuid"], profile)
        #     iterator += 1
        #     print(f"{iterator}/4693 done")
        #
        # print("Initialisation done!")


token = lib.get_lamastibot_token()
bot = Bot(command_prefix=commands.when_mentioned_or("!"), description="Commandes Lamastibot", pm_help=True)
bot.run(token)
