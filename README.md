# Lamastibot

This public repository contains the public version of Corobizar's Lamastibot (Discord bot).

## Requirements

```
python==3.6.*
discord.py-rewrite
requests
pytz
mysqlclient

```

## Config.txt

This repository needs a `config.txt` file in root directory which would contain those informations

```python
# Discord tokens

lamastibot_token=LamastibotDiscordToken
fourasticot_token=FourasticotDiscordToken

# Twitch App Logs

client_id=TwitchClientId
secret_key=TwitchSecretKey

# DB credentials

db_host=DataBaseHost
db_token=DataBaseToken
```

## License

```
Lamastibot
Copyright (C) 2018  Sakiut

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

contact@sakiut.fr
```
