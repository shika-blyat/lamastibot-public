"""
Lamastibot
Copyright (C) 2018  Sakiut

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

contact@sakiut.fr
"""

from discord.ext import commands
import discord.utils


def is_owner_check(message):
    return message.author.id == 187565415512276993


def is_owner():
    return commands.check(lambda ctx: is_owner_check(ctx.message))


def is_channel(channel_id):
    def predicate(ctx):
        return ctx.channel.id == channel_id
    return commands.check(predicate)


def check_permissions(ctx, perms):
    msg = ctx.message
    if is_owner_check(msg):
        return True

    ch = msg.channel
    author = msg.author
    resolved = ch.permissions_for(author)
    return all(getattr(resolved, name, None) == value for name, value in perms.items())


def role_or_permissions(ctx, check, **perms):
    if check_permissions(ctx, perms):
        return True

    ch = ctx.message.channel
    author = ctx.author
    if isinstance(ch, (discord.DMChannel, discord.GroupChannel)):
        return False  # can't have roles in PMs

    role = discord.utils.find(check, author.roles)
    if role is None:
        raise commands.CommandError("You need a special role to do this! (admin, Gardien)")
    return True


def gardien_or_permissions():
    def predicate(ctx):
        return role_or_permissions(ctx, lambda r: r.id == 251419879951958027, manage_roles=True)
    return commands.check(predicate)


def admin_or_permissions():
    def predicate(ctx):
        return role_or_permissions(ctx, lambda r: r.id == 220310157513457664, administrator=True)
    return commands.check(predicate)


def nsfw_channel():
    # noinspection PyUnresolvedReferences
    def predicate(ctx):
        if not (isinstance(ctx.channel,
                           (discord.DMChannel, discord.GroupChannel)) or "nsfw" in ctx.channel.name.casefold()):
            raise ChannelError("This command can only be used in `nsfw` channels!")
        return True

    return commands.check(predicate)


def no_pm():
    def predicate(ctx):
        if ctx.command.name == "help":
            return True
        if ctx.guild is None:
            raise commands.NoPrivateMessage('This command cannot be used in private messages.')
        return True

    return commands.check(predicate)
