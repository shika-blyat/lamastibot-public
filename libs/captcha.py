"""
Lamastibot
Copyright (C) 2018  Sakiut

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

contact@sakiut.fr
"""

from PIL import Image, ImageDraw, ImageFont
import random
import string


def generate(limit: int=250):

    cap_list = []

    for x in range(limit):
        cap_list.append(''.join(random.choices(string.ascii_lowercase + string.digits, k=12)))

    for cap in cap_list:
        img = Image.new('RGB', (120, 30), color="white")

        d = ImageDraw.Draw(img)
        font = ImageFont.truetype("arial.ttf", 15)

        d.text((10, 10), cap, font=font, fill=(0, 0, 0))
        img.save(f'./data/captcha_source/captcha_{cap}.png')

    with open("./data/captcha_source/listing.txt", 'w') as f:
        for x in cap_list:
            f.write(f"{x}\n")
        f.close()


def get_listing():
    with open("./data/captcha_source/listing.txt", 'r') as f:
        listing = f.read().splitlines()
        f.close()
    return listing


def get_captcha(captcha_id):
    return f"./data/captcha_source/captcha_{captcha_id}.png"
