"""
Lamastibot
Copyright (C) 2018  Sakiut

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

contact@sakiut.fr
"""

import discord
import requests
import pickle
import uuid
import random

from operator import itemgetter
from datetime import datetime

from libs.lib import get_db_token as gdt, get_db_host as gdh


token = gdt()
host = gdh()
header = {'authorization': f'token {token}'}


def get_profile(user=None, user_id=None):

    if user is not None:
        user_id = user.id

    q = requests.get(f"{host}/api/profile/{user_id}/", headers=header)

    if q.status_code == 200:
        result = q.json()
    elif q.status_code == 404:
        return None
    else:
        raise LookupError("Cannot join database")

    return result


def create_profile(user):

    data = {
        "name": f"{user.name.encode('ascii', 'ignore').decode()}#{user.discriminator}",
        "discord_uuid": user.id,
        "level": 0,
        "xp_points": 0,
        "date": datetime.now(),
        "last_message_date": user.joined_at,
        "coins": 0,
    }

    requests.post(f"{host}/api/profile/", headers=header, data=data)
    return get_profile(user)


def update_profile(user_id, data):

    r = requests.put(f"{host}/api/profile/{user_id}/", headers=header, data=data)
    return r.status_code


def get_all_profiles():

    results = []
    page_nbr = 1

    while page_nbr != 0:
        q = requests.get(f"{host}/api/profile/?page={page_nbr}", headers=header)

        if q.status_code == 200:
            result = q.json()
        elif q.status_code == 404:
            return None
        else:
            raise LookupError("Cannot join database")

        results += result['results']
        if result['next'] is None:
            page_nbr = 0
        else:
            page_nbr += 1

    return results


def delete_profile(discord_uuid):

    q = requests.delete(f"{host}/api/profile/{discord_uuid}/", headers=header)
    return q.status_code


def create_entry(bot, user, sanction, channel, author, date, reason=None, comment=None):
    profile = get_profile(user)
    if profile is None:
        create_profile(user)

    comment = "Aucun" if comment is None else comment
    reason = "Non spécifiée" if reason is None else reason

    data = {
        "target": f"{host}/api/profile/{user.id}/",
        "reason": reason,
        "comment": comment,
        "author": author,
        "sanction": sanction,
        "channel": channel,
        "modified_date": date,
        "date": date,
    }

    debug_embed = None

    if bot.debug:
        debug_embed = discord.Embed()
        debug_embed.colour = 0xff0000
        debug_embed.set_thumbnail(url=user.avatar_url)
        debug_embed.title = f"New report entry concerning {user}"
        debug_embed.add_field(name="Sanction", value=sanction)
        debug_embed.add_field(name="Channel", value=channel)
        debug_embed.add_field(name="Author", value=author)
        debug_embed.add_field(name="Reason", value=sanction)
        debug_embed.add_field(name="Comment", value=comment)
        debug_embed.add_field(name="Date", value=date)

    requests.post(f"{host}/api/report_entry/", headers=header, data=data)

    return debug_embed if bot.debug else 0


def get_entries(user):
    link = f"{host}/api/profile/{user.id}/"

    q = requests.get(f"{host}/api/report_entry/", headers=header)

    if q.status_code == 200:
        result = q.json()
    elif q.status_code == 404:
        return None
    else:
        raise LookupError("Cannot join database")

    results = result['results']

    user_entries = list(filter(lambda x: x['target'] == link, results))

    return user_entries


def get_top():

    q = requests.get(f"{host}/api/profile/?ordering=-xp_points", headers=header)

    if q.status_code == 200:
        result = q.json()
    else:
        raise LookupError("Cannot join database")

    return result["results"]


def get_xp_blocked():
    with open('data/exp_blocked.data', 'rb') as f:
        unpickler = pickle.Unpickler(f)
        data = unpickler.load()
        f.close()
    return data


def save_xp_blocked(data):
    with open('data/exp_blocked.data', 'wb') as f:
        pickler = pickle.Pickler(f)
        pickler.dump(data)
        f.close()


def add_temp_sanction(user, sanction, date, until, channel=None):
    profile = get_profile(user)
    if profile is None:
        create_profile(user)

    _uuid = uuid.uuid4()

    data = {
        "target": f"{host}/api/profile/{user.id}/",
        "sanction": sanction,
        "uuid": _uuid,
        "channel": channel,
        "date": date,
        "until": until,
    }

    requests.post(f"{host}/api/temp_sanction/", headers=header, data=data)
    return _uuid


def get_temp_sanctions():
    q = requests.get(f"{host}/api/temp_sanction/", headers=header)

    if q.status_code == 200:
        result = q.json()
    else:
        raise LookupError("Cannot join database")

    results = result['results']

    return results


def delete_temp_sanction(_uuid):

    q = requests.delete(f"{host}/api/temp_sanction/{_uuid}/", headers=header)
    return q.status_code


def get_user(sanction):
    link = sanction['target']
    q = requests.get(link, headers=header)

    if q.status_code == 200:
        result = q.json()
    elif q.status_code == 404:
        return None
    else:
        raise LookupError("Cannot join database")

    return result


def add_xp(user, amount):
    profile = get_profile(user)
    if profile is None:
        profile = create_profile(user)
    profile["xp_points"] += amount
    update_profile(user.id, profile)


########################################################################################################################
#                                                     BUG REPORTS                                                      #
########################################################################################################################


def create_bug_report(author, date, title, content):

    report_id = uuid.uuid4()

    data = {
        "author": f"{host}/api/profile/{author.id}/",
        "report_id": report_id,
        "date": date,
        "title": title,
        "content": content,
        "status": "NEW",
    }

    requests.post(f"{host}/api/bug_report/", headers=header, data=data)
    return report_id


########################################################################################################################
#                                                   SCHEDULED EVENTS                                                   #
########################################################################################################################

def create_scheduled_event(start_date, end_date, role_id=None):

    title = f"Event #{random.randint(0,1000)}"

    data = {
        "title": title,
        "role_id": role_id,
        "start_date": start_date,
        "end_date": end_date,
    }

    requests.post(f"{host}/api/scheduled_event/", headers=header, data=data)


########################################################################################################################
#                                                      DB XP BOOST                                                     #
########################################################################################################################

def save_boost(ctx, xp_boost):

    data = {
        "boost": xp_boost,
        "date": ctx.message.created_at,
        "user": ctx.author.id
    }

    with open('data/xp_boost.data', 'wb') as f:
        pickler = pickle.Pickler(f)
        pickler.dump(data)
        f.close()


def get_boost():
    with open('data/xp_boost.data', 'rb') as f:
        unpickler = pickle.Unpickler(f)
        data = unpickler.load()
        f.close()
    return data
